package main

import (
	"errors"
	"net/url"
	"time"
)

type dataRange struct {
	start, end time.Time
}

func parseParams(query url.Values) (dataRange, error) {
	if len(query) != 2 {
		return dataRange{}, errors.New("incorrect number of parameters in url")
	}

	startDateStr, found := query["start_date"]
	if !found {
		return dataRange{}, errors.New("no start_date in params")
	}
	if len(startDateStr) != 1 {
		return dataRange{}, errors.New("start_date occurs in params more than once")
	}

	endDateStr, found := query["end_date"]
	if !found {
		return dataRange{}, errors.New("no end_date in params")
	}
	if len(endDateStr) != 1 {
		return dataRange{}, errors.New("end_date occurs in params more than once")
	}

	startDate, err := time.Parse(layout, startDateStr[0])
	if err != nil {
		return dataRange{}, errors.New("incorrect start_date param")
	}

	endDate, err := time.Parse(layout, endDateStr[0])
	if err != nil {
		return dataRange{}, errors.New("incorrect end_date param")
	}

	if endDate.Before(startDate) {
		return dataRange{}, errors.New("end_date is earlier than start_date")
	}

	return dataRange{startDate, endDate}, nil
}
