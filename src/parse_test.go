package main

import (
	"net/url"
	"testing"
	"time"
)

func prepareQuery(s, e string) url.Values {
	v := url.Values{}
	v["start_date"] = []string{s}
	v["end_date"] = []string{e}

	return v
}

func toStr(t time.Time) string {
	return t.Format(layout)
}

func TestParseParams_InputIsCorrect(t *testing.T) {
	s, e := "2020-01-24", "2020-01-26"
	input := prepareQuery(s, e)

	dr, err := parseParams(input)
	if err != nil {
		t.Fatalf("Returned error from correct date queries, error:%s", err.Error())
	}

	if toStr(dr.start) != s || toStr(dr.end) != e {
		t.Fatal("Wrong conversion from string to time")
	}
}

func TestParseParams_StartDateAfterEndDate(t *testing.T) {
	s, e := "2020-01-28", "2020-01-26"
	input := prepareQuery(s, e)

	_, err := parseParams(input)
	if err == nil {
		t.Fatal("Failed to return error")
	}

	msg := "end_date is earlier than start_date"

	if err.Error() != msg {
		t.Fatalf("Returned error \"%s\" instead of \"%s\"", err.Error(), msg)
	}
}

func TestParseParams_StartDateOccursMoreThanOnce(t *testing.T) {
	input := url.Values{"start_date": []string{"2020-01-28", "2020-01-26"},
		"end_date": []string{"2020-01-28"}}

	_, err := parseParams(input)
	if err == nil {
		t.Fatal("Failed to return error")
	}

	msg := "start_date occurs in params more than once"

	if err.Error() != msg {
		t.Fatalf("Returned error \"%s\" instead of \"%s\"", err.Error(), msg)
	}
}
func TestParseParams_EndDateOccursMoreThanOnce(t *testing.T) {
	input := url.Values{"end_date": []string{"2020-01-28", "2020-01-26"},
		"start_date": []string{"2020-01-28"}}

	_, err := parseParams(input)
	if err == nil {
		t.Fatal("Failed to return error")
	}

	msg := "end_date occurs in params more than once"

	if err.Error() != msg {
		t.Fatalf("Returned error \"%s\" instead of \"%s\"", err.Error(), msg)
	}
}
func TestParseParams_NoStartDate(t *testing.T) {
	input := url.Values{"abcd": []string{"2020-01-28"},
		"end_date": []string{"2020-01-28"}}

	_, err := parseParams(input)
	if err == nil {
		t.Fatal("Failed to return error")
	}

	msg := "no start_date in params"

	if err.Error() != msg {
		t.Fatalf("Returned error \"%s\" instead of \"%s\"", err.Error(), msg)
	}
}
func TestParseParams_NoEndDate(t *testing.T) {
	input := url.Values{"abcd": []string{"2020-01-28"},
		"start_date": []string{"2020-01-28"}}

	_, err := parseParams(input)
	if err == nil {
		t.Fatal("Failed to return error")
	}

	msg := "no end_date in params"

	if err.Error() != msg {
		t.Fatalf("Returned error \"%s\" instead of \"%s\"", err.Error(), msg)
	}
}
func TestParseParams_AdditionalParam(t *testing.T) {
	input := url.Values{"start_date": []string{"2020-01-28"},
		"end_date": []string{"2020-01-28"},
		"abcd":     []string{"2020-01-28"}}

	_, err := parseParams(input)
	if err == nil {
		t.Fatal("Failed to return error")
	}

	msg := "incorrect number of parameters in url"

	if err.Error() != msg {
		t.Fatalf("Returned error \"%s\" instead of \"%s\"", err.Error(), msg)
	}
}

func TestParseParams_WrongDateFormat(t *testing.T) {
	type TestParams struct {
		urlValues url.Values
		errorMsg  string
	}

	params := []TestParams{
		TestParams{
			url.Values{"start_date": []string{"2020-01-28"}, "end_date": []string{"2020_01_28"}},
			"incorrect end_date param"},
		TestParams{
			url.Values{"start_date": []string{"2020_01-28"}, "end_date": []string{"2020-01-28"}},
			"incorrect start_date param"},
		TestParams{
			url.Values{"start_date": []string{"aaaa"}, "end_date": []string{"2020-01-28"}},
			"incorrect start_date param"},
		TestParams{
			url.Values{"start_date": []string{"2020-01-28"}, "end_date": []string{"2020-02-30"}},
			"incorrect end_date param"}}

	for _, p := range params {
		_, err := parseParams(p.urlValues)
		if err == nil {
			t.Fatal("Failed to return error")
		}

		if err.Error() != p.errorMsg {
			t.Fatalf("Returned error \"%s\" instead of \"%s\"", err.Error(), p.errorMsg)
		}
	}
}
