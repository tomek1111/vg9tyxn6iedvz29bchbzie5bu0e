package main

import (
	"encoding/json"
	"log"
	"os"
	"strconv"
	"time"
)

const (
	maxBuffer = 100
	layout    = "2006-01-02"
)

var config struct {
	API_KEY             string
	PORT                string
	CONCURRENT_REQUESTS int
}

func readVarStr(val, def string) string {
	test, found := os.LookupEnv(val)
	if found {
		return test
	}
	return def
}

func readVarInt(val string, def int) int {
	test, found := os.LookupEnv(val)
	if !found {
		return def
	}

	concInt, err := strconv.Atoi(test)
	if err != nil {
		return def
	}

	return concInt
}

func readEnvVars() {
	config.API_KEY = readVarStr("API_KEY", "DEMO_KEY")
	config.PORT = readVarStr("PORT", "8080")
	config.CONCURRENT_REQUESTS = readVarInt("CONCURRENT_REQUESTS", 5)

	log.Printf("Server will start with os variables:\n")
	log.Printf("\tAPI_KEY = %s\n", config.API_KEY)
	log.Printf("\tPORT = %s\n", config.PORT)
	log.Printf("\tCONCURRENT_REQUESTS = %d\n", config.CONCURRENT_REQUESTS)
}

func getJSONErr(msg string) []byte {
	var j struct {
		Error string `json:"error"`
	}
	j.Error = msg
	res, _ := json.Marshal(&j)
	return res
}

func toJSON(urls []string) ([]byte, error) {
	var rj struct {
		Urls []string `json:"urls"`
	}
	rj.Urls = urls

	return json.Marshal(&rj)
}

func getNumOfDays(start, end time.Time) int {
	return int(end.Sub(start).Hours()/24) + 1
}
