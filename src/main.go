package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"
)

type outputData struct {
	url string
	e   error
}

type queryData struct {
	date string
	out  chan outputData
}

var queries = make(chan queryData, maxBuffer)

func worker() {
	for query := range queries {
		url := fmt.Sprintf("https://api.nasa.gov/planetary/apod?api_key=%s&date=%s", config.API_KEY, query.date)

		var client = &http.Client{
			Timeout: time.Second * 5,
		}

		resp, err := client.Get(url)

		if err != nil {
			query.out <- outputData{e: err}
			continue
		}

		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			errMsg := fmt.Sprintf("Response from remote API returned status %s", resp.Status)
			query.out <- outputData{e: errors.New(errMsg)}
			continue
		}

		var pic struct {
			URL string `json:"url"`
		}

		err = json.NewDecoder(resp.Body).Decode(&pic)

		if err != nil {
			query.out <- outputData{e: errors.New("Failed to decode json body from reomote API response")}
			continue
		}

		query.out <- outputData{url: pic.URL}
	}
}

func pictures(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	p, err := parseParams(req.URL.Query())
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write(getJSONErr(err.Error()))
		return
	}

	numUrls := getNumOfDays(p.start, p.end)

	out := make(chan outputData, maxBuffer)
	go func() {
		for date := p.start; !date.After(p.end); date = date.AddDate(0, 0, 1) {
			queries <- queryData{date.Format(layout), out}
		}
	}()

	urls := make([]string, 0, numUrls)
	var firstError error

	// If at least one requested url could not be retrived, return the first encountered error
	for i := 0; i < numUrls; i++ {
		outData := <-out

		if outData.e != nil {
			firstError = outData.e
		} else {
			urls = append(urls, outData.url)
		}
	}

	if firstError != nil {
		w.WriteHeader(http.StatusInternalServerError)
		errMsg := fmt.Sprintf("Could not download urls from remote API: %s", firstError.Error())
		w.Write(getJSONErr(errMsg))
		return
	}

	jsonURL, err := toJSON(urls)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(getJSONErr("Could not created json with urls returned from remote API"))
		return
	}

	w.Write(jsonURL)
}

func main() {
	readEnvVars()

	// Launch a worker pool of CONCURRENT_REQUESTS workers
	// All single date requests from all client requests will be processed
	// by this pool
	for i := 0; i < config.CONCURRENT_REQUESTS; i++ {
		go worker()
	}

	http.HandleFunc("/pictures", pictures)
	log.Fatal(http.ListenAndServe(":"+config.PORT, nil))
}
