Docker image can be built using:

```
docker image build -t url-collector .
```


and run using:

```
docker container run -p 8080:8080 url-collector
```


Unit tests can be run:
```
cd src; go test -v
```