FROM golang:1.14-alpine AS build

WORKDIR /src/
COPY src/*.go /src/

RUN CGO_ENABLED=0 go build -o /bin/url-collector

FROM scratch
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /bin/url-collector /bin/url-collector

ENTRYPOINT ["/bin/url-collector"]
